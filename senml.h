#ifndef __SENML_H__
#define __SENML_H__

#ifdef REST_MAX_CHUNK_SIZE
	#define SENML_MAX_LEN REST_MAX_CHUNK_SIZE
#else
	#define SENML_MAX_LEN 64
#endif

#include "contiki-net.h"
#include "net/ip/uip.h"
#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"


typedef struct {
	char* n;
	int v;
} measure_t;

int senml_build(char* buffer,int buf_len,char* bn,char* bu ,measure_t* mv, int ml);

void get_id(char* id,int id_len);


#endif //__SENML_H__
