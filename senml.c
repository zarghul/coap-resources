#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "senml.h"

static char *format_start = 
"{"
	"\"bn\":\"%s\","
	"\"bu\":\"%s\","
	"\"e\":[",
*format_elems =
		"{"
			"\"n\":\"%s\","
			"\"v\":%i"
		"}",
*format_elems_separator=
		",",
*format_end = 
	"]"
"}";


int 
senml_build(char* buffer,int buf_len,char* bn,char* bu ,measure_t* mv, int ml)
{
	int pos = snprintf(buffer,buf_len,format_start,bn,bu);
	int i;
	for(i = 0; i<ml-1;i++)
	{
		pos += snprintf(buffer+pos,buf_len-pos,format_elems,mv[i].n,mv[i].v);
		pos += snprintf(buffer+pos,buf_len-pos,format_elems_separator);
	}
	pos += snprintf(buffer+pos,buf_len-pos,format_elems,mv[ml-1].n,mv[ml-1].v);
	pos += snprintf(buffer+pos,buf_len-pos,format_end);
	return pos;
}

void get_id(char* id,int id_len)
{
	char buffer[64];
	int len = 64;

	int i;
	uint8_t state;
	uip_ipaddr_t* addr;

	for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
		state = uip_ds6_if.addr_list[i].state;
		if(uip_ds6_if.addr_list[i].isused &&
		state == ADDR_PREFERRED) 
		{
			addr = &uip_ds6_if.addr_list[i].ipaddr;
			break;
		}
	}

	int n = 0;

	uint16_t a;
	int f;
	for(i = 0, f = 0; i < sizeof(uip_ipaddr_t); i += 2) 
	{
		a = (addr->u8[i] << 8) + addr->u8[i + 1];
		if(a == 0 && f >= 0) 
		{
			if(f++ == 0) 
			{

				n += snprintf(buffer+n,len,"::");
			}
		} 
		else 
		{
			if(f > 0) 
			{
				f = -1;
			} 
			else if(i > 0) 
			{
				n += snprintf(buffer+n,len,":");
			}
			n += snprintf(buffer+n,len,"%x", a);
		}
	}
	memcpy(id,buffer+n-id_len,id_len+1);
}

