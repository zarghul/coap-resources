#include <stdlib.h>
#include <string.h>
#include "rest-engine.h"

#include "senml.h"

static void temp_get_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void temp_put_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void temp_periodic_handler(void);

/*
 * For data larger than REST_MAX_CHUNK_SIZE (e.g., when stored in flash) resources must be aware of the buffer limitation
 * and split their responses by themselves. To transfer the complete resource through a TCP stream or CoAP's blockwise transfer,
 * the byte offset where to continue is provided to the handler as int32_t pointer.
 * These chunk-wise resources must set the offset value to its new position or -1 of the end is reached.
 * (The offset for CoAP's blockwise transfer can go up to 2'147'481'600 = ~2047 M for block size 2048 (reduced to 1024 in observe-03.)
 */
PERIODIC_RESOURCE(temperature_senml,
         "title=\"Temperature sensor\";",
         temp_get_handler,
         temp_put_handler,
         temp_put_handler,
         NULL,
		 30 * CLOCK_SECOND,
		 temp_periodic_handler);


#define CHUNKS_TOTAL    64*3

static int buf_len = 0;

#define T_LEN 3
measure_t temp[T_LEN];


#define ID_LEN 3
char id[ID_LEN+1];

#define BUF_LEN REST_MAX_CHUNK_SIZE*T_LEN
static char my_buf[BUF_LEN];

#define NAME_MAX_LEN 10
static char sensor_names[T_LEN*NAME_MAX_LEN];

static int init = 0;

static void
temp_get_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
	//one time inititialization
	if(init == 0){
		int i = 0;
		for(;i<T_LEN;i++)
		{
			snprintf(sensor_names+i*NAME_MAX_LEN,NAME_MAX_LEN,"sensor%d",i);
			temp[i].n = sensor_names+i*NAME_MAX_LEN;
			temp[i].v = 20000;
		}	
		get_id(id,ID_LEN);
		printf("### MOTE ID: %s\n",id);
		init = 1;
	}

	printf("### GET request received for temperature\n");

	//begin of blockwise transfer
	if(*offset==0){

		const char* value = NULL;
		if(REST.get_query_variable(request, "sensor", &value)) 
		{
			int sensor_id = atoi(value);
			printf("### QUERY requested sensor %d\n",sensor_id);
			if(sensor_id<0 || sensor_id>=T_LEN)
			{
				printf("### sensor %d does not exists\n",sensor_id);
    			REST.set_response_status(response, REST.status.BAD_REQUEST);
				return;
			}
			buf_len = senml_build(my_buf,BUF_LEN,id,"°mC",&temp[sensor_id],1);
		}
		else
		{
			buf_len = senml_build(my_buf,BUF_LEN,id,"°mC",temp,T_LEN);
		}

	}

	int len = 0;
	if(buf_len - *offset > preferred_size)
	{
		len = preferred_size;
	}
	else
	{
		len = buf_len - *offset;
	}

	memcpy(buffer,my_buf+*offset,len);

	char str [SENML_MAX_LEN+1];
	memcpy(str,buffer,len);
	str[len]='\0';

	REST.set_header_content_type(response, REST.type.APPLICATION_JSON);
	REST.set_response_payload(response, buffer, len);
	REST.set_header_max_age(response, 10);

	*offset += len;

	/* Signal end of resource representation. */
	if(*offset >= buf_len) {
		*offset = -1;
	}
}

static void
temp_put_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
	printf("### PUT request received for temperature\n");
	char* bytes = 0;
	int len = REST.get_request_payload(request, &bytes);
	printf("### PUT/POST received value: %s\n",bytes);

	const char* value = NULL;
	if(REST.get_query_variable(request, "sensor", &value)) 
	{
		int sensor_id = atoi(value);
		printf("### QUERY requested sensor %d\n",sensor_id);
		if(sensor_id<0 || sensor_id>=T_LEN)
		{
			printf("### sensor %d does not exists\n",sensor_id);
			REST.set_response_status(response, REST.status.BAD_REQUEST);
			return;
		}
		temp[sensor_id].v = atoi(bytes);
	}
	else
	{
		buf_len = senml_build(my_buf,BUF_LEN,id,"°mC",temp,T_LEN);
		int i;
		for(i=0;i<T_LEN;i++)
		{
			temp[i].v = atoi(bytes);
		}
	}
    REST.set_response_status(response, REST.status.CHANGED);
}

static void
temp_periodic_handler()
{
	//rnd will be -range<rnd<+range
	static int range = 1000;


	int scale = RANDOM_RAND_MAX/(2*range);
	int rnd = 0;

	int i;
	for(i=0;i<T_LEN;i++)
	{
		rnd = random_rand()/scale - range;
		temp[i].v += rnd;
		if(temp[i].v > 30000)
		{
			temp[i].v = 30000;
		}
		else if(temp[i].v<-30000)
		{
			temp[i].v = -30000;
		}
	}
}
