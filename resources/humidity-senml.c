#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#include "lib/random.h"

#include "rest-engine.h"

#include "er-coap.h"
#include "../senml.h"

static void humidity_get_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void humidity_put_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void humidity_periodic_handler(void);

PERIODIC_RESOURCE(humidity_senml,
		"title=\"Humidity Sensor\";obs",
		humidity_get_handler,
		humidity_put_handler,
		humidity_put_handler,
		NULL,
		30 * CLOCK_SECOND,
		humidity_periodic_handler);



static measure_t humidity = {
	"sensor1",
	50
};




int init = 0;
#define ID_LEN 3
char id[ID_LEN+1];

static void
humidity_get_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
	/*
	 * For minimal complexity, request query and options should be ignored for GET on observable resources.
	 * Otherwise the requests must be stored with the observer list and passed by REST.notify_subscribers().
	 * This would be a TODO in the corresponding files in contiki/apps/erbium/!
	 */

	//first time initialize vars
	if(!init)
	{
		init = 1;
		random_init(0);
		get_id(id,ID_LEN);
		printf("### MOTE ID: %s\n",id);
	}

	printf("### GET request received for humidity\n");

	int len = senml_build(buffer,REST_MAX_CHUNK_SIZE,id,"%",&humidity,1);
	int actual_len = len;
	if(actual_len > REST_MAX_CHUNK_SIZE)
	{
		 actual_len = REST_MAX_CHUNK_SIZE;
		 printf("### RESPONSE TRUNCATED TO REST_MAX_CHUNK_SIZE (%d)\n",REST_MAX_CHUNK_SIZE);
	}

	REST.set_header_content_type(response, REST.type.APPLICATION_JSON);
	REST.set_header_max_age(response, humidity_senml.periodic->period / CLOCK_SECOND);
	REST.set_response_payload(response, buffer, actual_len);


	/* The REST.subscription_handler() will be called for observable resources by the REST framework. */
}

static void
humidity_put_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
	printf("### PUT request received for humidity\n");
	char* bytes = 0;
    REST.set_response_status(response, REST.status.CHANGED);
	int len = REST.get_request_payload(request, &bytes);
	printf("### PUT/POST received value: %s\n",bytes);
	humidity.v = atoi(bytes);

}

/*
 * Additionally, a handler function named [resource name]_handler must be implemented for each PERIODIC_RESOURCE.
 * It will be called by the REST manager process with the defined period.
 */
static void
humidity_periodic_handler()
{
	//rnd will be -range<rnd<+range
	static int range = 2;


	int scale = RANDOM_RAND_MAX/(2*range);
	int rnd = random_rand()/scale - range;

	humidity.v += rnd;
	if(humidity.v > 100)
	{
		humidity.v = 100;
	}
	else if(humidity.v<0)
	{
		humidity.v = 0;
	}

	REST.notify_subscribers(&humidity_senml);
}
